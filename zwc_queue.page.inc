<?php

/**
 * @file
 * Contains zwc_queue.page.inc.
 *
 * Page callback for ZWCQueue entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for ZWCQueue templates.
 *
 * Default template: zwc_queue.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zwc_queue(array &$variables) {
  // Fetch ZWCQueue Entity Object.
  $zwc_queue = $variables['elements']['#zwc_queue'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
