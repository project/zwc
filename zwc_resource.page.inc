<?php

/**
 * @file
 * Contains zwc_resource.page.inc.
 *
 * Page callback for ZWCResource entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for ZWCResource templates.
 *
 * Default template: zwc_resource.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zwc_resource(array &$variables) {
  // Fetch ZWCResource Entity Object.
  $zwc_resource = $variables['elements']['#zwc_resource'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
