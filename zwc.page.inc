<?php

/**
 * @file
 * Contains zwc.page.inc.
 *
 * Page callback for ZWC entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for ZWC templates.
 *
 * Default template: zwc.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zwc(array &$variables) {
  // Fetch ZWC Entity Object.
  $zwc = $variables['elements']['#zwc'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
