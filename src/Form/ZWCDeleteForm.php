<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ZWC entities.
 *
 * @ingroup zwc
 */
class ZWCDeleteForm extends ContentEntityDeleteForm {


}
