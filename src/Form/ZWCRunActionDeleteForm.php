<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ZWC Run Action entities.
 *
 * @ingroup zwc
 */
class ZWCRunActionDeleteForm extends ContentEntityDeleteForm {


}
