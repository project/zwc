<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ZWCResource entities.
 *
 * @ingroup zwc
 */
class ZWCResourceDeleteForm extends ContentEntityDeleteForm {


}
