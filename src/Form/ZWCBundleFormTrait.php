<?php

namespace Drupal\zwc\Form;

/**
 * Trait ZWCBundleFormTrait
 * @package Drupal\zwc\Form
 */
trait ZWCBundleFormTrait {
  /**
   * @return array
   */
  private function getConfigOptions($entity_type) {
    $plugins = \Drupal::service('plugin.manager.zwc_config_generator')->getConfigPlugins($entity_type);
    $options = [];
    foreach ($plugins as $id => $plugin) {
      $options[$id] = $plugin['label'];
    }
    return $options;
  }

}
