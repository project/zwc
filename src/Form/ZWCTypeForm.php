<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ZWCTypeForm.
 */
class ZWCTypeForm extends EntityForm {

  use ZWCBundleFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $zwc_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $zwc_type->label(),
      '#description' => $this->t("Label for the ZWC type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $zwc_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\zwc\Entity\ZWCType::load',
      ],
      '#disabled' => !$zwc_type->isNew(),
    ];

    $form['config_generator'] = [
      '#type' => 'select',
      '#title' => $this->t('Configuration Generator'),
      '#description' => $this->t('Use the selected method to turn resources into Zeomine configuration code. Usually the same name as the entity type.'),
      '#default_value' => $zwc_type->config_generator,
      '#options' => $this->getConfigOptions('zwc'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $zwc_type = $this->entity;
    $status = $zwc_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label ZWC type.', [
          '%label' => $zwc_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label ZWC type.', [
          '%label' => $zwc_type->label(),
        ]));
    }
    //$form_state->setRedirectUrl($zwc_type->toUrl('collection'));
  }

}
