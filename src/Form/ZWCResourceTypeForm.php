<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ZWCResourceTypeForm.
 */
class ZWCResourceTypeForm extends EntityForm {

  use ZWCBundleFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $zwc_resource_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $zwc_resource_type->label(),
      '#description' => $this->t("Label for the ZWCResource type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $zwc_resource_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\zwc\Entity\ZWCResourceType::load',
      ],
      '#disabled' => !$zwc_resource_type->isNew(),
    ];

    $form['config_generator'] = [
      '#type' => 'select',
      '#title' => $this->t('Configuration Generator'),
      '#description' => $this->t('Use the selected method to turn resources into Zeomine configuration code. Usually the same name as the entity type.'),
      '#default_value' => $zwc_resource_type->config_generator,
      '#options' => $this->getConfigOptions('zwc_resource'),
      '#required' => TRUE,
    ];

    $form['zwc_conf_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Resource Type'),
      '#description' => $this->t('The type of Zeomine Resource'),
      '#default_value' => $zwc_resource_type->zwc_conf_type,
      '#options' => [
        'auth' => $this->t('Authenticator'),
        'config' => $this->t('Configuration Source'),
        'crawler' => $this->t('Crawler'),
        'crawl_manager' => $this->t('Crawl Manager'),
        'db' => $this->t('Database'),
        'processor' => $this->t('Data Processor'),
        'reporter' => $this->t('Reporter'),
        'custom' => $this->t('Custom Resource'),
      ],
      '#required' => TRUE,
    ];
    $form['zwc_conf_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Python class name'),
      '#description' => $this->t('The "class" in "from [location] import [class]"'),
      '#default_value' => $zwc_resource_type->zwc_conf_class,
      '#required' => TRUE,
    ];
    $form['zwc_conf_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Import Location'),
      '#description' => $this->t('Python import location: The "location" in "from [location] import [class]'),
      '#default_value' => $zwc_resource_type->zwc_conf_location,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $zwc_resource_type = $this->entity;
    $status = $zwc_resource_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label ZWCResource type.', [
          '%label' => $zwc_resource_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label ZWCResource type.', [
          '%label' => $zwc_resource_type->label(),
        ]));
    }
    //$form_state->setRedirectUrl($zwc_resource_type->toUrl('collection'));
  }

}
