<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ZWCQueue entities.
 *
 * @ingroup zwc
 */
class ZWCQueueDeleteForm extends ContentEntityDeleteForm {


}
