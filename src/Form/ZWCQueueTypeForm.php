<?php

namespace Drupal\zwc\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ZWCQueueTypeForm.
 */
class ZWCQueueTypeForm extends EntityForm {

  use ZWCBundleFormTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $zwc_queue_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $zwc_queue_type->label(),
      '#description' => $this->t("Label for the ZWCQueue type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $zwc_queue_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\zwc\Entity\ZWCQueueType::load',
      ],
      '#disabled' => !$zwc_queue_type->isNew(),
    ];

    $form['config_generator'] = [
      '#type' => 'select',
      '#title' => $this->t('Configuration Generator'),
      '#description' => $this->t('Use the selected method to turn resources into Zeomine configuration code. Usually the same name as the entity type.'),
      '#default_value' => $zwc_queue_type->config_generator,
      '#options' => $this->getConfigOptions('zwc_queue'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $zwc_queue_type = $this->entity;
    $status = $zwc_queue_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label ZWCQueue type.', [
          '%label' => $zwc_queue_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label ZWCQueue type.', [
          '%label' => $zwc_queue_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($zwc_queue_type->toUrl('collection'));
  }

}
