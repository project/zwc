<?php

namespace Drupal\zwc\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;

/**
 * Class ValidZWCQueueUUIDAccessCheck
 * @package Drupal\zwc\Access
 */
class ValidZWCQueueUUIDAccessCheck implements AccessInterface {

  /**
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $uuid = $route_match->getParameter('zwc_queue_uuid');
    if (is_string($uuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) === 1)) {
      $s = \Drupal::entityTypeManager()->getStorage('zwc_queue')->loadByProperties(['uuid' => $uuid]);
      return !empty($s) ? AccessResult::allowed() : AccessResult::forbidden();
    }
    // Parameter is not a valid UUID
    else {
      return AccessResult::forbidden();
    }
  }

}
