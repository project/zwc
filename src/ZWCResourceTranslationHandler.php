<?php

namespace Drupal\zwc;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for zwc_resource.
 */
class ZWCResourceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
