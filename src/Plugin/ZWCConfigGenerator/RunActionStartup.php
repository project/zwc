<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWCRunAction;
use Drupal\zwc\Plugin\ZWCConfigGeneratorBase;


/**
 * Class RunActionStartup
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "run_startup",
 *   label = "Startup",
 *   description = "runs the startup() command on all Zeomine plugins.",
 *   entity_types = {
 *     "zwc_run_action"
 *   }
 * )
 */
class RunActionStartup extends ZWCConfigGeneratorBase {

  /**
   * @param ZWCRunAction $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    // The startup and shutdown functions have no configuration at this time.
    return ['startup' => []];
  }

}