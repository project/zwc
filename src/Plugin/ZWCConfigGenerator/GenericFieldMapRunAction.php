<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWCResource;
use Drupal\zwc\Entity\ZWCRunAction;
use Drupal\zwc\Plugin\ZWCConfigGeneratorBase;


/**
 * Class GenericFieldMapRunAction
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "run_generic_field_map",
 *   label = "Generic Run Action with basic field mapping",
 *   description = "Generic run action with basic field mapping",
 *   entity_types = {
 *     "zwc_run_action"
 *   }
 * )
 */
class GenericFieldMapRunAction extends ZWCConfigGeneratorBase {

  /**
   * @param ZWCRunAction $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    $config = [];
    $entity_array = $entity->toArray();
    if (isset($entity_array['field_conf_location'])) {
      unset($entity_array['field_conf_location']);
    }
    foreach ($entity_array as $key => $values) {
      if (strpos($key, 'field_') !== FALSE && count($values)) {
        $filter_key = str_replace('field_', '', $key);
        $data_keys = explode('__', $filter_key);
        $data_keys = array_reverse($data_keys);
        $add = [];
        if (count($values) === 1) {
          $value = array_shift($values);
          if (isset($value['target_id'])) {
            $add = ZWCResource::load($value['target_id'])->uuid();
          }
        }
        else {
          foreach ($values as $value) {
            if (isset($value['target_id'])) {
              $add[] = ZWCResource::load($value['target_id'])->uuid();
            }
          }
        }
        // If we have data, add it to the config.
        if ($add) {
          foreach($data_keys as $dk) {
            $add = [$dk => $add];
          }
          foreach ($add as $key => $value) {
            if(isset($config[$key])) {
              $config[$key] = array_merge($config[$key], $add[$key]);
            }
            else {
              $config[$key] = $add[$key];
            }
          }
        }
      }
    }

    return $config;
  }

}