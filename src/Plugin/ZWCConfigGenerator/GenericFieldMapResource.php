<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWCResource;
use Drupal\zwc\Entity\ZWCResourceType;
use Drupal\zwc\Plugin\ZWCConfigGeneratorBase;


/**
 * Class ResourceTextExtraction
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "resource_generic_field_map",
 *   label = "Generic resource with basic field mapping",
 *   description = "Removes 'field_' from Drupal fields.",
 *   entity_types = {
 *     "zwc_resource"
 *   }
 * )
 */
class GenericFieldMapResource extends ZWCConfigGeneratorBase {

  /**
   * @param $value
   * @return |null
   */
  public static function readValue($value) {
    // Most common case
    if (isset($value['value'])) {
      return $value['value'];
    }
    // Double Field, return dict of first:second
    // If only one is populated, this returns first:first or second:second.
    if (isset($value['first']) || isset($value['second'])) {
      if (isset($value['first']) && isset($value['second'])) {
        return [$value['first'] => $value['second']];
      }
      elseif (isset($value['first'])) {
        return [$value['first'] => $value['first']];
      }
      // we reach this point because 'second' was set.
      else {
        return [$value['second'] => $value['second']];
      }
    }
    // If we reach this point, return NULL.
    return NULL;
  }

  /**
   * @param ZWCResource $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    $resource_type = ZWCResourceType::load($entity->bundle());
    // Create config container
    $config = [
      'type' => $resource_type->zwc_conf_type,
      'class' => $resource_type->zwc_conf_class,
      'location' => $resource_type->zwc_conf_location,
    ];
    // Use the conf-location if it is set.
    $conf_location = $entity->field_conf_location->value;
    if ($conf_location) {
      $config['conf_location'] = $conf_location;
      return $config;
    }
    // Build configs from fields.
    else {
      $config['conf'] = [];
      $entity_array = $entity->toArray();
      if (isset($entity_array['field_conf_location'])) {
        unset($entity_array['field_conf_location']);
      }
      foreach ($entity_array as $key => $values) {
        if (strpos($key, 'field_') !== FALSE && count($values)) {
          $cardinality = $entity->getFieldDefinition($key)->getFieldStorageDefinition()->getCardinality();
          $filter_key = str_replace('field_', '', $key);
          $data_keys = explode('__', $filter_key);
          $data_keys = array_reverse($data_keys);
          $add = [];
          if ($cardinality === 1) {
            $value = array_shift($values);
            $add = self::readValue($value);
          }
          else {
            foreach ($values as $value) {
              $add[] = self::readValue($value);
            }
          }
          // If we have data, add it to the config.
          if ($add) {
            foreach($data_keys as $dk) {
              $add = [$dk => $add];
            }
            foreach ($add as $key => $value) {
              if(isset($config['conf'][$key])) {
                $config['conf'][$key] = array_merge($config['conf'][$key], $add[$key]);
              }
              else {
                $config['conf'][$key] = $add[$key];
              }
            }
          }
        }
      }

      return $config;
    }

  }

}