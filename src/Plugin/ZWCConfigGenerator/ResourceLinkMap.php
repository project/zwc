<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWCResource;
use Drupal\zwc\Entity\ZWCResourceType;
use Drupal\zwc\Plugin\ZWCConfigGeneratorBase;


/**
 * Class ResourceLinkMap
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "crawl_manager_linkmap",
 *   label = "LinkMap",
 *   description = "Generates configuration for a LinkMap resource",
 *   entity_types = {
 *     "zwc_resource"
 *   }
 * )
 */
class ResourceLinkMap extends GenericFieldMapResource {

  /**
   * @param ZWCResource $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    $config = parent::generateConfiguration($entity);
    // Link selector logic
    if(isset($config['conf']['links']['selectors']) && !empty($config['conf']['links']['selectors'])) {
      $entity_array = $entity->toArray();
      $selectors = [];
      foreach ($entity_array['field_links__selectors'] as $selector) {
        // Add only if all data was entered.
        if (isset($selector['first']) && isset($selector['second'])) {
          $selectors[$selector['first']][] = $selector['second'];
        }
      }
      // Overwrite earlier value
      $config['conf']['links']['selectors'] = $selectors;
    }
    // Crawl delay logic
    if(isset($config['conf']['crawl_delay']) && !empty($config['conf']['crawl_delay'])) {
      $cd = explode(':',$config['conf']['crawl_delay']);
      //Clean any spaces or non-numeric entries.
      $cd_cleaned = [];
      foreach ($cd as $part) {
        $part = trim($part);
        if (is_numeric($part)) {
          $cd_cleaned[] = $part;
        }
      }
      if (!count($cd_cleaned)) {
        $config['conf']['crawl_delay'] = 0;
      }
      elseif (count($cd_cleaned) === 1) {
        $config['conf']['crawl_delay'] = $cd_cleaned[0];
      }
      elseif (count($cd_cleaned) === 2) {
        sort($cd_cleaned);
        $config['conf']['crawl_delay'] = ['min' => $cd_cleaned[0], 'max' => $cd_cleaned[1], 'step' => 1];
      }
      else {
        $step = $cd_cleaned[2];
        $minmax = array_slice($cd_cleaned,0,2);
        sort($minmax);
        $config['conf']['crawl_delay'] = ['min' => $minmax[0], 'max' => $minmax[1], 'step' => $step];
      }

    }

      return $config;
  }

}