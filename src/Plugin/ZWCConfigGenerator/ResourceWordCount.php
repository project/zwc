<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWCResource;
use Drupal\zwc\Entity\ZWCResourceType;

/**
 * Class ResourceWordCount
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "resource_word_count",
 *   label = "Word Count resource",
 *   description = "Removes 'field_' from Drupal fields, and updates 'reports' to the right format",
 *   entity_types = {
 *     "zwc_resource"
 *   }
 * )
 */
class ResourceWordCount extends GenericFieldMapResource {

  /**
   * @param ZWCResource $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    $config = parent::generateConfiguration($entity);
    // Note: since there might be two reports with the same scope key, one may have overwritten the other.
    if(isset($config['conf']['reports']) && !empty($config['conf']['reports'])) {
      $entity_array = $entity->toArray();
      $reports = [];
      foreach ($entity_array['field_reports'] as $report) {
        // Add only if all data was entered.
        if (isset($report['first']) && isset($report['second'])) {
          $reports[] = [
            'scope' => $report['first'],
            'query' => $report['second'],
          ];
        }
      }
      // Overwrite earlier value
      $config['conf']['reports'] = $reports;
    }

    return $config;
  }

}