<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWCRunAction;
use Drupal\zwc\Plugin\ZWCConfigGeneratorBase;


/**
 * Class ShutdownRunAction
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "run_shutdown",
 *   label = "Shutdown",
 *   description = "runs the shutdown() command on all Zeomine plugins.",
 *   entity_types = {
 *     "zwc_run_action"
 *   }
 * )
 */
class RunActionShutdown extends ZWCConfigGeneratorBase {

  /**
   * @param ZWCRunAction $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    // The startup and shutdown functions have no configuration at this time.
    return ['shutdown' => []];
  }

}