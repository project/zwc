<?php

namespace Drupal\zwc\Plugin\ZWCConfigGenerator;

use Drupal\zwc\Entity\ZWC;
use Drupal\zwc\Entity\ZWCResource;
use Drupal\zwc\Entity\ZWCRunAction;
use Drupal\zwc\Plugin\ZWCConfigGeneratorBase;


/**
 * Class ZWCConfig
 * @package Drupal\zwc\Plugin\ZWCConfigGenerator
 *
 * @ZWCConfigGenerator (
 *   id = "zwc_config",
 *   label = "Zeomine Config",
 *   description = "Config generator",
 *   entity_types = {
 *     "zwc"
 *   }
 * )
 */
class ZWCConfig extends ZWCConfigGeneratorBase {

  /**
   * @param ZWC $entity
   * @return array
   */
  public static function generateConfiguration($entity) {
    // Create config container
    $config = [];
    // Add metadata
    $meta_name = $entity->field_metadata_name->value;
    if ($meta_name) {
      $config['metadata']['name'] = $meta_name;
    }

    // Add Resources
    $config['resources'] = [];
    $resources = $entity->get('field_resources')->getValue();
    foreach ($resources as $i => $resource) {
      $resource = ZWCResource::load($resource['target_id']);
      $config['resources'][$resource->uuid()] = $resource->generateConfigurationArray();
    }


    // Add Run Actions
    $config['run'] = [];
    $run_actions = $entity->get('field_run_actions')->getValue();
    foreach ($run_actions as $i => $run_action) {
      $run_action = ZWCRunAction::load($run_action['target_id']);
      $config['run'][] = $run_action->generateConfigurationArray();
    }

    // Return the completed configuration
    return $config;
  }

}