<?php

namespace Drupal\zwc\Plugin\Menu;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\zwc\Entity\ZWC;

/**
 * Provides route parameters needed to link to Zeomine Config API
 */
class ZWCTab extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $zwc = $route_match->getParameter('zwc');
    $zwc_resource = $route_match->getParameter('zwc_resource');
    $zwc_run_action = $route_match->getParameter('zwc_run_action');
    if ($zwc) {
      /* @var $zwc ZWC */
      return ['zwc_uuid' => $zwc->uuid(), 'entity_type' => 'zwc'];
    }
    elseif($zwc_resource) {
      return ['zwc_uuid' => $zwc_resource->uuid(), 'entity_type' => 'zwc_resource'];
    }
    elseif($zwc_run_action) {
      return ['zwc_uuid' => $zwc_run_action->uuid(), 'entity_type' => 'zwc_run_action'];
    }
    else {
      return ['zwc_uuid' => '0', 'entity_type' => 'zwc'];
    }
  }

}