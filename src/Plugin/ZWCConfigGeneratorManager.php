<?php

namespace Drupal\zwc\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the ZWC Config Generator plugin manager.
 */
class ZWCConfigGeneratorManager extends DefaultPluginManager {


  /**
   * Constructs a new ZWCConfigGeneratorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ZWCConfigGenerator', $namespaces, $module_handler, 'Drupal\zwc\Plugin\ZWCConfigGeneratorInterface', 'Drupal\zwc\Annotation\ZWCConfigGenerator');

    $this->alterInfo('zwc_zwc_config_generator_info');
    $this->setCacheBackend($cache_backend, 'zwc_zwc_config_generator_plugins');
  }

  public function getConfigPlugins($entity_type = 'zwc') {
    $plugins = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      if (in_array($entity_type, $definition['entity_types'])) {
        $plugins[$id] = $definition;
      }
    }
    return $plugins;
  }

}
