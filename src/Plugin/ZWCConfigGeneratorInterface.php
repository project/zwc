<?php

namespace Drupal\zwc\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for ZWC Config Generator plugins.
 */
interface ZWCConfigGeneratorInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
