<?php

namespace Drupal\zwc\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for ZWC Config Generator plugins.
 */
abstract class ZWCConfigGeneratorBase extends PluginBase implements ZWCConfigGeneratorInterface {

  // Add common methods and abstract methods for your plugin type here.
  public static function generateConfiguration($entity) {
    return [];
  }
}
