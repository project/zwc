<?php

namespace Drupal\zwc;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for zwc.
 */
class ZWCTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
