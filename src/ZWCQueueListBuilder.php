<?php

namespace Drupal\zwc;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of ZWCQueue entities.
 *
 * @ingroup zwc
 */
class ZWCQueueListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ZWCQueue ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\zwc\Entity\ZWCQueue $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.zwc_queue.edit_form',
      ['zwc_queue' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
