<?php

namespace Drupal\zwc;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\zwc\Entity\ZWCRunActionType;


/**
 * Provides dynamic permissions for ZWC Run Action of different types.
 *
 * @ingroup zwc
 *
 */
class ZWCRunActionPermissions{

  use StringTranslationTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   *   The ZWCRunAction by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach (ZWCRunActionType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\zwc\Entity\ZWCRunActionType $type
   *   The ZWCRunAction type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(ZWCRunActionType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "$type_id create entities" => [
        'title' => $this->t('Create new %type_name entities', $type_params),
      ],
      "$type_id view own entities" => [
        'title' => $this->t('View own %type_name entities', $type_params),
      ],
      "$type_id view any entities" => [
        'title' => $this->t('View any %type_name entities', $type_params),
      ],
      "$type_id edit own entities" => [
        'title' => $this->t('Edit own %type_name entities', $type_params),
      ],
      "$type_id edit any entities" => [
        'title' => $this->t('Edit any %type_name entities', $type_params),
      ],
      "$type_id delete own entities" => [
        'title' => $this->t('Delete own %type_name entities', $type_params),
      ],
      "$type_id delete any entities" => [
        'title' => $this->t('Delete any %type_name entities', $type_params),
      ],
    ];
  }

}
