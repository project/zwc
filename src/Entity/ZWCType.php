<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ZWC type entity.
 *
 * @ConfigEntityType(
 *   id = "zwc_type",
 *   label = @Translation("ZWC type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\zwc\ZWCTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zwc\Form\ZWCTypeForm",
 *       "edit" = "Drupal\zwc\Form\ZWCTypeForm",
 *       "delete" = "Drupal\zwc\Form\ZWCTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zwc\ZWCTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zwc_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zwc",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zwc/zwc_type/{zwc_type}",
 *     "add-form" = "/admin/structure/zwc/zwc_type/add",
 *     "edit-form" = "/admin/structure/zwc/zwc_type/{zwc_type}/edit",
 *     "delete-form" = "/admin/structure/zwc/zwc_type/{zwc_type}/delete",
 *     "collection" = "/admin/structure/zwc/zwc_type"
 *   }
 * )
 */
class ZWCType extends ConfigEntityBundleBase implements ZWCTypeInterface {

  /**
   * The ZWC type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The ZWC type label.
   *
   * @var string
   */
  protected $label;

}
