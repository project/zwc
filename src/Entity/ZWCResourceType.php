<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ZWCResource type entity.
 *
 * @ConfigEntityType(
 *   id = "zwc_resource_type",
 *   label = @Translation("ZWCResource type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\zwc\ZWCResourceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zwc\Form\ZWCResourceTypeForm",
 *       "edit" = "Drupal\zwc\Form\ZWCResourceTypeForm",
 *       "delete" = "Drupal\zwc\Form\ZWCResourceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zwc\ZWCResourceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zwc_resource_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zwc_resource",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zwc_resource_type/{zwc_resource_type}",
 *     "add-form" = "/admin/structure/zwc_resource_type/add",
 *     "edit-form" = "/admin/structure/zwc_resource_type/{zwc_resource_type}/edit",
 *     "delete-form" = "/admin/structure/zwc_resource_type/{zwc_resource_type}/delete",
 *     "collection" = "/admin/structure/zwc_resource_type"
 *   }
 * )
 */
class ZWCResourceType extends ConfigEntityBundleBase implements ZWCResourceTypeInterface {

  /**
   * The ZWCResource type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The ZWCResource type label.
   *
   * @var string
   */
  protected $label;

}
