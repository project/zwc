<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining ZWCQueue entities.
 *
 * @ingroup zwc
 */
interface ZWCQueueInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the ZWCQueue name.
   *
   * @return string
   *   Name of the ZWCQueue.
   */
  public function getName();

  /**
   * Sets the ZWCQueue name.
   *
   * @param string $name
   *   The ZWCQueue name.
   *
   * @return \Drupal\zwc\Entity\ZWCQueueInterface
   *   The called ZWCQueue entity.
   */
  public function setName($name);

  /**
   * Gets the ZWCQueue creation timestamp.
   *
   * @return int
   *   Creation timestamp of the ZWCQueue.
   */
  public function getCreatedTime();

  /**
   * Sets the ZWCQueue creation timestamp.
   *
   * @param int $timestamp
   *   The ZWCQueue creation timestamp.
   *
   * @return \Drupal\zwc\Entity\ZWCQueueInterface
   *   The called ZWCQueue entity.
   */
  public function setCreatedTime($timestamp);

}
