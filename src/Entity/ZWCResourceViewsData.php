<?php

namespace Drupal\zwc\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for ZWCResource entities.
 */
class ZWCResourceViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
