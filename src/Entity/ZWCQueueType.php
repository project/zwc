<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ZWCQueue type entity.
 *
 * @ConfigEntityType(
 *   id = "zwc_queue_type",
 *   label = @Translation("ZWCQueue type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\zwc\ZWCQueueTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zwc\Form\ZWCQueueTypeForm",
 *       "edit" = "Drupal\zwc\Form\ZWCQueueTypeForm",
 *       "delete" = "Drupal\zwc\Form\ZWCQueueTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zwc\ZWCQueueTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zwc_queue_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zwc_queue",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zwc_queue_type/{zwc_queue_type}",
 *     "add-form" = "/admin/structure/zwc_queue_type/add",
 *     "edit-form" = "/admin/structure/zwc_queue_type/{zwc_queue_type}/edit",
 *     "delete-form" = "/admin/structure/zwc_queue_type/{zwc_queue_type}/delete",
 *     "collection" = "/admin/structure/zwc_queue_type"
 *   }
 * )
 */
class ZWCQueueType extends ConfigEntityBundleBase implements ZWCQueueTypeInterface {

  /**
   * The ZWCQueue type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The ZWCQueue type label.
   *
   * @var string
   */
  protected $label;

}
