<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining ZWCQueue type entities.
 */
interface ZWCQueueTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
