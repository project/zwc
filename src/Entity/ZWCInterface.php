<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining ZWC entities.
 *
 * @ingroup zwc
 */
interface ZWCInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the ZWC name.
   *
   * @return string
   *   Name of the ZWC.
   */
  public function getName();

  /**
   * Sets the ZWC name.
   *
   * @param string $name
   *   The ZWC name.
   *
   * @return \Drupal\zwc\Entity\ZWCInterface
   *   The called ZWC entity.
   */
  public function setName($name);

  /**
   * Gets the ZWC creation timestamp.
   *
   * @return int
   *   Creation timestamp of the ZWC.
   */
  public function getCreatedTime();

  /**
   * Sets the ZWC creation timestamp.
   *
   * @param int $timestamp
   *   The ZWC creation timestamp.
   *
   * @return \Drupal\zwc\Entity\ZWCInterface
   *   The called ZWC entity.
   */
  public function setCreatedTime($timestamp);

}
