<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining ZWCResource type entities.
 */
interface ZWCResourceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
