<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ZWC Run Action type entity.
 *
 * @ConfigEntityType(
 *   id = "zwc_run_action_type",
 *   label = @Translation("ZWC Run Action type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\zwc\ZWCRunActionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\zwc\Form\ZWCRunActionTypeForm",
 *       "edit" = "Drupal\zwc\Form\ZWCRunActionTypeForm",
 *       "delete" = "Drupal\zwc\Form\ZWCRunActionTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zwc\ZWCRunActionTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "zwc_run_action_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "zwc_run_action",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/zwc_run_action_type/{zwc_run_action_type}",
 *     "add-form" = "/admin/structure/zwc_run_action_type/add",
 *     "edit-form" = "/admin/structure/zwc_run_action_type/{zwc_run_action_type}/edit",
 *     "delete-form" = "/admin/structure/zwc_run_action_type/{zwc_run_action_type}/delete",
 *     "collection" = "/admin/structure/zwc_run_action_type"
 *   }
 * )
 */
class ZWCRunActionType extends ConfigEntityBundleBase implements ZWCRunActionTypeInterface {

  /**
   * The ZWC Run Action type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The ZWC Run Action type label.
   *
   * @var string
   */
  protected $label;

}
