<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining ZWC Run Action type entities.
 */
interface ZWCRunActionTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
