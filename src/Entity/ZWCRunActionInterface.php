<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining ZWC Run Action entities.
 *
 * @ingroup zwc
 */
interface ZWCRunActionInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the ZWC Run Action name.
   *
   * @return string
   *   Name of the ZWC Run Action.
   */
  public function getName();

  /**
   * Sets the ZWC Run Action name.
   *
   * @param string $name
   *   The ZWC Run Action name.
   *
   * @return \Drupal\zwc\Entity\ZWCRunActionInterface
   *   The called ZWC Run Action entity.
   */
  public function setName($name);

  /**
   * Gets the ZWC Run Action creation timestamp.
   *
   * @return int
   *   Creation timestamp of the ZWC Run Action.
   */
  public function getCreatedTime();

  /**
   * Sets the ZWC Run Action creation timestamp.
   *
   * @param int $timestamp
   *   The ZWC Run Action creation timestamp.
   *
   * @return \Drupal\zwc\Entity\ZWCRunActionInterface
   *   The called ZWC Run Action entity.
   */
  public function setCreatedTime($timestamp);

}
