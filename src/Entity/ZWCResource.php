<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the ZWCResource entity.
 *
 * @ingroup zwc
 *
 * @ContentEntityType(
 *   id = "zwc_resource",
 *   label = @Translation("ZWC Resource"),
 *   bundle_label = @Translation("ZWC Resource type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\zwc\ZWCResourceListBuilder",
 *     "views_data" = "Drupal\zwc\Entity\ZWCResourceViewsData",
 *     "translation" = "Drupal\zwc\ZWCResourceTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\zwc\Form\ZWCResourceForm",
 *       "add" = "Drupal\zwc\Form\ZWCResourceForm",
 *       "edit" = "Drupal\zwc\Form\ZWCResourceForm",
 *       "delete" = "Drupal\zwc\Form\ZWCResourceDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\zwc\ZWCResourceHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\zwc\ZWCResourceAccessControlHandler",
 *   },
 *   base_table = "zwc_resource",
 *   data_table = "zwc_resource_field_data",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer zwcresource entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/zwc_resource/{zwc_resource}",
 *     "add-page" = "/zwc_resource/add",
 *     "add-form" = "/zwc_resource/add/{zwc_resource_type}",
 *     "edit-form" = "/zwc_resource/{zwc_resource}/edit",
 *     "delete-form" = "/zwc_resource/{zwc_resource}/delete",
 *     "collection" = "/admin/structure/zwc_resource",
 *   },
 *   bundle_entity_type = "zwc_resource_type",
 *   field_ui_base_route = "entity.zwc_resource_type.edit_form"
 * )
 */
class ZWCResource extends ContentEntityBase implements ZWCResourceInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * @return mixed
   */
  public function generateConfigurationArray() {
    $plugins = \Drupal::service('plugin.manager.zwc_config_generator')->getConfigPlugins('zwc_resource');
    $generator = ZWCResourceType::load($this->bundle())->config_generator;
    return $plugins[$generator]['class']::generateConfiguration($this);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the ZWCResource entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the ZWCResource entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the ZWCResource is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
