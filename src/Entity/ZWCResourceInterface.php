<?php

namespace Drupal\zwc\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining ZWCResource entities.
 *
 * @ingroup zwc
 */
interface ZWCResourceInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the ZWCResource name.
   *
   * @return string
   *   Name of the ZWCResource.
   */
  public function getName();

  /**
   * Sets the ZWCResource name.
   *
   * @param string $name
   *   The ZWCResource name.
   *
   * @return \Drupal\zwc\Entity\ZWCResourceInterface
   *   The called ZWCResource entity.
   */
  public function setName($name);

  /**
   * Gets the ZWCResource creation timestamp.
   *
   * @return int
   *   Creation timestamp of the ZWCResource.
   */
  public function getCreatedTime();

  /**
   * Sets the ZWCResource creation timestamp.
   *
   * @param int $timestamp
   *   The ZWCResource creation timestamp.
   *
   * @return \Drupal\zwc\Entity\ZWCResourceInterface
   *   The called ZWCResource entity.
   */
  public function setCreatedTime($timestamp);

}
