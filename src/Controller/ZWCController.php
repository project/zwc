<?php

namespace Drupal\zwc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\zwc\Entity\ZWC;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ZWCController.
 */
class ZWCController extends ControllerBase {

//  /**
//   * @param $zwc_uuid
//   * @return JsonResponse|Response
//   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
//   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
//   */
//  public function getZWC($zwc_uuid) {
//    $zwc = ZWC::loadFromUUID($zwc_uuid);
//    if ($zwc) {
//      // @TODO make cacheable, add metadata
//      return JsonResponse::create($zwc->generateConfigurationArray());
//    }
//    else {
//      return JsonResponse::create('not found');
//    }
//  }

  /**
   * @param string $entity_type
   * @param string $zwc_uuid
   * @return bool|\Drupal\Core\Entity\EntityInterface|mixed|JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getZWC($entity_type, $zwc_uuid) {
    if (is_string($zwc_uuid) && (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $zwc_uuid) === 1)) {
      $z = \Drupal::entityTypeManager()->getStorage($entity_type)->loadByProperties(['uuid' => $zwc_uuid]);
      if(!empty($z)) {
        $zwc_component = array_shift($z);
        return JsonResponse::create($zwc_component->generateConfigurationArray());
      }
    }
    // We did not find a valid UUID that points to a ZWC Resource or Run Action.
    return JsonResponse::create('not found');
  }

  /**
   * Get_zwc_from_queue.
   *
   * @return array
   *   Return configuration.
   */
  public function getZWCFromQueue($queue_uuid) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: get_zwc_from_queue with parameter(s): $queue_uuid'),
    ];
  }

}
