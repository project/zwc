<?php

namespace Drupal\zwc\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ZWC Config Generator item annotation object.
 *
 * @see \Drupal\zwc\Plugin\ZWCConfigGeneratorManager
 * @see plugin_api
 *
 * @Annotation
 */
class ZWCConfigGenerator extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
