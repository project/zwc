<?php

namespace Drupal\zwc;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of ZWC Run Action entities.
 *
 * @ingroup zwc
 */
class ZWCRunActionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ZWC Run Action ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\zwc\Entity\ZWCRunAction $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.zwc_run_action.edit_form',
      ['zwc_run_action' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
