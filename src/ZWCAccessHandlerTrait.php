<?php

namespace Drupal\zwc;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Trait ZWCAccessHandlerTrait
 * @package Drupal\zwc
 */
trait ZWCAccessHandlerTrait {

  /**
   * Test for given 'own' permission.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $operation
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return string|null
   *   The permission string indicating it's allowed.
   */
  protected function checkOwnOrAny(EntityInterface $entity, $operation, AccountInterface $account) {
    $status = $entity->isPublished();
    $uid = $entity->getOwnerId();

    $is_own = $account->isAuthenticated() && $account->id() == $uid;
    $own_any = 'own';
    if (!$is_own) {
      $own_any = 'any';
    }

    $bundle = $entity->bundle();

    $ops = [
      'create' => '%bundle add %own_any entities',
      'view unpublished' => '%bundle view %own_any unpublished entities',
      'view' => '%bundle view %own_any entities',
      'update' => '%bundle edit %own_any entities',
      'delete' => '%bundle delete %own_any entities',
    ];
    $permission = strtr($ops[$operation], ['%bundle' => $bundle, '%own_any' => $own_any]);

    if ($operation === 'view unpublished') {
      if (!$status && $account->hasPermission($permission)) {
        return $permission;
      }
      else {
        return NULL;
      }
    }
    if ($account->hasPermission($permission)) {
      return $permission;
    }

    return NULL;
  }

}