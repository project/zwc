<?php

namespace Drupal\zwc;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the ZWCResource entity.
 *
 * @see \Drupal\zwc\Entity\ZWCResource.
 */
class ZWCResourceAccessControlHandler extends EntityAccessControlHandler {

  use ZWCAccessHandlerTrait;

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    /** @var \Drupal\zwc\Entity\ZWCResourceInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          $permission = $this->checkOwnOrAny($entity, 'view unpublished', $account);
          if (!empty($permission)) {
            return AccessResult::allowed();
          }

          return AccessResult::allowedIfHasPermission($account, 'view unpublished zwcresource entities');
        }

        $permission = $this->checkOwnOrAny($entity, $operation, $account);
        if (!empty($permission)) {
          return AccessResult::allowed();
        }

        return AccessResult::allowedIfHasPermission($account, 'view published zwcresource entities');

      case 'update':

        $permission = $this->checkOwnOrAny($entity, $operation, $account);
        if (!empty($permission)) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIfHasPermission($account, 'edit zwcresource entities');

      case 'delete':

        $permission = $this->checkOwnOrAny($entity, $operation, $account);
        if (!empty($permission)) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIfHasPermission($account, 'delete zwcresource entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add zwcresource entities');
  }

}
