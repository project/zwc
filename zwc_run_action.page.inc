<?php

/**
 * @file
 * Contains zwc_run_action.page.inc.
 *
 * Page callback for ZWC Run Action entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for ZWC Run Action templates.
 *
 * Default template: zwc_run_action.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_zwc_run_action(array &$variables) {
  // Fetch ZWCRunAction Entity Object.
  $zwc_run_action = $variables['elements']['#zwc_run_action'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
