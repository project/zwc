<?php

namespace Drupal\zwc\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the zwc module.
 */
class ZWCControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "zwc ZWCController's controller functionality",
      'description' => 'Test Unit for module zwc and controller ZWCController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests zwc functionality.
   */
  public function testZWCController() {
    // Check that the basic functions of module zwc.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
